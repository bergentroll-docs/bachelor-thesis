BUILD = xelatex -shell-escape report.tex

all: report.tex
	$(BUILD)
	$(BUILD)

once:
	$(BUILD)

clean:
	rm -f *.{pdf,log,aux,out,toc,dvi}
	rm -rf _minted-report/
